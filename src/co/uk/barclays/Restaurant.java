package co.uk.barclays;

import java.sql.Statement;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Restaurant {
    public static ArrayList<Restaurant> all = new ArrayList<>();
    public static void init() {
        try {
            Menu.init();
            Statement createTable = DB.conn.createStatement();
            createTable.execute("CREATE TABLE IF NOT EXISTS restaurants (id INTEGER PRIMARY KEY, name TEXT, imageURL TEXT);");
            Statement getRestaurants = DB.conn.createStatement();
            PreparedStatement getMenus = DB.conn.prepareStatement("SELECT * FROM menus WHERE restaurant_id = ?;");
            ResultSet restaurants = getRestaurants.executeQuery("SELECT * FROM restaurants;");
            while(restaurants.next()) {
                int id = restaurants.getInt(1);
                String name = restaurants.getString(2);
                String imageURL = restaurants.getString(3);
                Restaurant restaurant = new Restaurant(id, name, imageURL);
                getMenus.setInt(1, restaurant.getId());
                ResultSet menus = getMenus.executeQuery();
                while(menus.next()) {
                    Menu menu = new Menu(menus.getInt(1), menus.getString(2));
                    restaurant.menus.add(menu);
                }
            }
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    private int id;
    private String name;
    private String imageURL;
    private ArrayList<Menu> menus = new ArrayList<>();

    public Restaurant(String name, String imageURL) {
        this.name = name;
        this.imageURL = imageURL;
        try {
            PreparedStatement insertRestaurant = DB.conn.prepareStatement("INSERT INTO restaurants (name, imageURL) VALUES (?, ?);");
            insertRestaurant.setString(1, this.name);
            insertRestaurant.setString(2, this.imageURL);
            insertRestaurant.executeUpdate();
            this.id = insertRestaurant.getGeneratedKeys().getInt(1);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        Restaurant.all.add(this);
    }

    public Restaurant(int id, String name, String imageURL) {
        this.id = id;
        this.name = name;
        this.imageURL = imageURL;
        Restaurant.all.add(this);    
    }

    public int getId() {
        return id;
    }

    public void addMenu(String menuTitle) {
        Menu menu = new Menu(menuTitle);
        this.menus.add(menu);
    }
}
