package co.uk.barclays;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class Menu {
    private int id;
    private String title;

    public static void init() {
        try {
            Statement createTable = DB.conn.createStatement();
            createTable.execute("CREATE TABLE IF NOT EXISTS menus (id INTEGER PRIMARY KEY, restaurant_id INTEGER, title TEXT);");
        } catch(SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public Menu(String title) {
        this.title = title;
        try {
            PreparedStatement insert = DB.conn.prepareStatement("INSERT INTO menus (title) VALUES (?);");
            insert.setString(1, this.title);
            insert.executeUpdate();
            this.id = insert.getGeneratedKeys().getInt(1);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public Menu(int id, String title) {
        this.id = id;
        this.title = title;
    }
}
